<!-- Футер -->
<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<!-- <a href="policy.html">Политика конфиденциальности</a> -->
					<?php 
						wp_nav_menu(
							[
								'theme_location' => 'bottom',
								'container' => '',
								'menu_class' => '',
								'menu_id' => '',
								'items_wrap' => '%3$s'
							]
						);
					?>
					<span class="accent-color">&copy; </span>
					Двери Жовнер, 2021
				</div>
			</div>
		</div>
	</div>
  <?php wp_footer(); ?>
</body>
</html>